<style type="text/css">
div{
    width: 320px;
}
span{
    display: inline-block;
    width: 30px;
    height: 18px;
    background-color: white;
    text-align: center;
    color: black;
    border: 1px solid grey;
}
.pair_found{
    border-color: red;
}
</style>
<div>

<?php

$visibility=true; // true - если хотим отображать массив визуально, false - если не хотим

// создаём массив, выводим на экран
$array=[];
for($i=0;$i<=99;$i++){
    $array[$i]=rand(-10,10);
    if($visibility){
        echo '<span>'.$array[$i].'</span>';
    }
}
?>
</div>
<hr>
<div>
<?php
// ищем все последовательные пары одинаковых значений, первый элемент пары красим красным border
$counter=0;
$code_collector='';
for($i=0;$i<=99;$i++) {
    if($i<=98){
        if($array[$i]===$array[$i+1]){
            if($visibility) {
                $code_collector .= '<span class="pair_found">' . $array[$i] . '</span>';
            }
            $counter++;
        }
        else{
            if($visibility) {
                $code_collector .= '<span>' . $array[$i] . '</span>';
            }
        }
    }else{
        if($visibility) {
            $code_collector .= '<span>' . $array[$i] . '</span>';
        }
    }

}
if($visibility) {
    echo $code_collector;
}
?>
</div>
<?php
// выводим кол-во последовательных пар
echo 'Кол-во последовательных пар равно: '.$counter.'.';
?>
