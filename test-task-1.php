<?php
// функция-обработчик для обрезки текста новости (180 символов по-умолчанию)
function news_text_shortener($text,$link,$shortener_limit=180){
    if(isset($text) || isset($link)){
        mb_regex_encoding('UTF-8');
        mb_internal_encoding('UTF-8');
        // лимит на длину выходного сообщения
        // переменная с текстом
        // регулярка для нахождения всевозможных открывающих и закрывающих тегов
        $regex='/(<[\w\d\s\~\!\@\"#\№\$\;\%\:\?\&\*\-\_\+\=\'\|\/\.\а-яА-Я]*>)/ui';
        // вычищаем теги
        $trimmed_text=preg_replace($regex,' ',$text);
        // убираем разнообразные лишние пробелы
        $trimmed_text=preg_replace(['/  /','/ \./','/ \,/','/« /','/ »/','/ "/','/" /'],[' ','.',',','«','»',' "','" '],$trimmed_text);
        // разбираем на слова
        $trimmed_text_splitted=explode(' ',$trimmed_text);
        $shortened_text='';
        // делаем обрезку по словам, чтобы длина текста оказалась меньше или равна $shortener_limit (лимит длины)
        foreach($trimmed_text_splitted as $word){
            if(strlen($shortened_text)+strlen($word)<=$shortener_limit){
                $shortened_text.=' '.$word;
            }
            else{
                break;
            }
        }
        // приписываем многоточие
        $shortened_text.=' ...';
        // снова разбираем, чтобы отобрать текст для ссылки
        $shortened_text=explode(' ',$shortened_text);
        // выбираем последние 2 слова, включая многоточие (это могут оказаться предлоги или союзы, например малозначительная и короткая для ссылки фраза "и из-за"
        $offset=count($shortened_text)-3;
        $link='<a href="'.$link.'">'.implode(' ',array_slice($shortened_text,$offset)).'</a>';
        // возвращаем результать (String)
        return implode(' ',array_slice($shortened_text,0,count($shortened_text)-3)).' '.$link;
    }
    else{
        // если текст или ссылка не были переданы - функция вернёт false
        return false;
    }
}
// текстовая переменная с текстом новости
$a='<div class="_21bQB"><div class="aRmfw" id="summary"><p>Главы России и <a data-article-text-tag="" href="/France/" class="j-article-text-tag j-article-text-tag-region biqpf-autotag" data-blocks="click_link_contentpage_region" target="_blank">Франции</a> <a data-article-text-tag="" href="/person/putin-vladimir/" class="j-article-text-tag j-article-text-tag-person biqpf-autotag" data-blocks="click_link_contentpage_person" target="_blank">Владимир Путин</a> и <a data-article-text-tag="" href="/person/makron-emmanyuel/" class="j-article-text-tag j-article-text-tag-person biqpf-autotag" data-blocks="click_link_contentpage_person" target="_blank">Эмманюэль Макрон</a> провели телефонные переговоры, в ходе которых обсудили конфликты, происходящие внутри <a data-article-text-tag="" href="/Ukraine/" class="j-article-text-tag j-article-text-tag-region biqpf-autotag" data-blocks="click_link_contentpage_region" target="_blank">Украины</a>, сообщает «<a data-blocks="click_link_contentpage" class="j-article-text-link" href="https://www.rambler.ru/transition/?redirect_url=https%3A%2F%2Fura.news%2Fnews%2F1052516931" target="_blank">Ура.ру</a>» со ссылкой на заявление <a data-article-text-tag="" href="/organization/press-sluzhba-administratsii-prezidenta-rf/" class="j-article-text-tag j-article-text-tag-organization biqpf-autotag" data-blocks="click_link_contentpage_organization" target="_blank">пресс-службы Кремля</a>.</p></div><div></div><div class="_14yH-"><div><div class=""><img alt="Франция поддержала Россию в конфликте с Украиной" class="lazy _10Yb9 lazyLoaded" data-src="https://news.store.rambler.ru/img/a936503159730fe43aacc4779ddbd7cf?img-1-resize=width%3A560%2Cheight%3A315%2Cfit%3Acover&amp;img-format=auto" src="https://news.store.rambler.ru/img/a936503159730fe43aacc4779ddbd7cf?img-1-resize=width%3A560%2Cheight%3A315%2Cfit%3Acover&amp;img-format=auto" data-was-processed="true" width="615" height="400"></div><div class="_2l-sL"><span>Фото:&nbsp;</span><a class="_1kuHT" href="https://ria.ru/?utm_medium=source&amp;utm_source=rnews" target="_blank" rel="noopener noreferrer nofollow">РИА Новости</a><span class="_2stMi">РИА Новости</span></div></div></div><div class="aRmfw"><p>Стороны выразили «неудовлетворение в связи с отсутствием подвижек в урегулировании внутриукраинского конфликта. Отмечено, что ситуация вокруг Донбасса даже ухудшается», — следует из сообщения.</p></div><div class="aRmfw"><p>Кроме того, российский лидер обратил внимание на провокационный характер масштабных учений, которые проводят в Черном море США и их союзники. Как считает Путин, это повышает напряженность между Москвой и <a data-article-text-tag="" href="/organization/nato/" class="j-article-text-tag j-article-text-tag-organization biqpf-autotag" data-blocks="click_link_contentpage_organization" target="_blank">НАТО</a>. «Президент России также акцентировал, что Киев продолжает свою деструктивную политику, направленную на срыв Минских соглашений», — отмечается в заявлении Кремля.</p></div><div class="aRmfw"><p>Также стороны обсудили миграционный кризис на границах Белоруссии и стран <a data-article-text-tag="" href="/organization/es/" class="j-article-text-tag j-article-text-tag-organization biqpf-autotag" data-blocks="click_link_contentpage_organization" target="_blank">Евросоюза</a>. Лидеры России и Франции сошлись во мнении, что сейчас важно ослабить напряжение, возникшее между Белоруссией и странами ЕС. В то же время Путин и Макрон не пришли к единой точке зрения в вопросе о причинах возникновения миграционного кризиса.</p></div><div class="aRmfw"><p>Париж выразил надежду, что разговор глав государств принесёт положительные результаты уже в ближайшие дни.</p></div><div class="_3gBpB _1N-gT"></div><div class="_3CJCL"><div class=""><div class="_1ErId _3LwKW LTCoz"><div class="_1cgiz"><div id="adfox-508991615" class="_1gx0Z" style="display:block"><div id="begun_block_508991615"><iframe src="//img01.ssp.rambler.ru/file.jsp?url=1SeH8IaaI3zqeqEcjwjJBCTcEGHd5aX2qE6FAGNqXiUVaMMyK0rLMoy3dE1DyedW**sD*fRqcllue8qtaryOG545xY6CLZQDY*g19*Xjs71Y27fCJ4EEFdJUo45Qrq*XeE-u5C5F7YpzEgkbrf4zzDHbVbXSuClhdFV*tZoR3LmeKSQcOC6eGgLWsbb9ODzPs0nT3bNxKbOdhNkrMZbJO2xwAhrXHgZAT6jtikC069IEZuT6DRV8S8vcYfVNjqRpx86ad3I20OvaeNh**jSWLS9bwz7mq8EDT-3B5lQ2VZHodICrshorCSiiXAKeurZMSksLBXU1PmxZKxM0jKI893bvhqBW3ckLv5-A8TUpzSz1V9jB1Vu0IaR7bAUWFdRUZljCeQqGaC4Zxqug*qTw9YGBB1*fXe*Ue99fpSGkhtTxYLNj*MPISrIaxUKdIfU8JFIP0Z0tUwdgFArsz5BSolnYW5qJMi6n3mXwWPkrtC7u69nTCXKA6hgH*eRCb1emjwTqQhAde1*PRvdCOYpAn8yinAu*FFvgJa8ddOk3hQi3g3TmGSf6aefJQwuTuWZMt1nTMlSJLnaqSnMYIup7UB4ozCTutgB9r7Gpc7qFqBtdpSPiMqieSkIzDXo5WrlREBBBnDsMT7M4G-rFMN8mt3NVQag9VDLnM3CbP4c71fmvvZQJqR9cV2hAMTi*Uao4LOQfYeGINqlilxduilKM2ZrL2DnVX3aFB6Ezp1t*HWEf3bD3WtCv8yDB7M9QATn9-D*P-N4j4Ev1Hr-fursA2APoj*BjapV6PHQtncpwG7thqWdCsFljIcDTbN6Cuwv1p81pQj4-*ll31T*LGubPGX6TDOKQBRQWgrmfGkrgiwMd0dSHn17M3aJq6o5pkIoYlqpYH8QkNNtFsLQw4DuVWlojW2OgCYSVVJdqGiXKJ6WzWkaK2PyZ2tXWcyQnniqRZFmTpzi8TL8p6DRdrWs3sAg81ygv5VMN67fjaoH3zikwwEe4xIplXrElMGLY*nScGEMDUtvTg9XcXHa1nGb8zbGIHMbZX1xDp4xjiZBDQ6TKhi*Y8VpRiFJZvUnO8ofK7zx-1wCnTCs2JwaJzUE-6qrYFkewCWoftc5Z4JWhS6rWBaKQ9vcjLFP0kvl2zBGadApI1LOfhrQW4oCPHq3j7vcse8CQPT86Mj3ZZmD6Xfc3s4qmxJAoF27tTerkcqlUgYC4t6lwh8LXDZ2GCywqqEOUnWAZl5uwkbYOpDL*XMsrvD2RMkkN0Mk16KoN2sojl24vjXBcHtZJybA8qDy5L1jwZbpPtR*ROmkOrXZT8dcMuQ6DJlL95J1Pus2uZ7zexE5Cz6vCT6PInvfOngJjsopBFwNP1nx3chr1D1EiGBwe-aJS4G1aqXA8aUTPbJ9zJzqXfAlkN-XsG6zUKR-4G9L8PBVBfQOL-0aAzTWnJCKUuCq9T4qDq3a1fPaOp7HtxhYzDLcoHZjGnkyjkCpiM4REjgInzWgVcwby7-d-7Rf0qF3uER5YVOUED9HFP5ugLSsbTjIft9iTWRclRIbcSn1ut1lMq4KTxUFBw7odHqWxoRpk-cL0nMqH-svLqezf45*vCodjTa86KJK1v7XfM1i75phn4Ije*2*fsI-CbbxqdayQPmv*nCv7HY1ZrHjkdQGg5LUmClpWD-7v9HC0H17gibtt3aaouycQ1TcKks18soWED6c2g9dfpdRb8vHRIYFJUx2vRdbSj3wJ1POODuBqfXYaocBSK7bYOex4-gcmhOklCb5KANhSXIHLnPTjY-JjbWvlzvPb6RFRKhhV0wPevYPAmvBdMq7Xt2Sya3XjzZ-*dUCIRm7BXZlMD95-K36dCUhSFDgasTa7AFgiahN-RtZABzPbNXLEiaRRh-t7axb8ecImlAxfM1yhbL2KQP2I7-BLfcfawVtvsnhFSn88JoglusPUIDRxkSd0wWyANRSem9KuEiXSy1GN8wBgSFTj0TOVGFe3vC53G0LzA9tPJpXu572tZMeBBO8QB4zVU3JzCb3PTpSz59m8LjzaYU-6jdbDuSC83bVR8JBMgRfmmjOuBoYpcvObsoE24RUZShPs5MZ9l*IJFbLjNc2my2EiXXpW7WkcdYFUVYj67b3UsPtsF6VHwoVze-syISqvY*uXJDlFiGypW0XPbfKXbsHBPBQbU5JITSCmowarzsOHwSIkiagLHQc*kh*zk*ULxo0gJWocy5*q*Y2LL7Seu3ScGVZa27pSVoYVZFhgZsYMOSdHsgIrE8gzi*um*L8svpB9TkmB4ReS7IynFkC*sEyrJTkCyU6ut7ZxsSI8i3dn2pZojuq-YU*TQSXqPLuwyOCyP95row9mqHZnwPQg7MZp3CbVzV1ghWDjbDhfQQhU*uEXTkMhTVrMWJxVX17FG3w6ZA4AqMr7MA3dNoxgR2NtiO09NK1F1CJGS2RABdTPI-3kcImo0udcPZV*uA4M0--IQZvHksOsD7W*f*NUZFzWUltISQpOyXjMwnKoMnCZy65e6TrLd4IEtMKPol3F1ECzFyogT0zgDEA6*WoUmT4GmYT3mu3HqNJk*Vo8pUPeyv*8X1cmynLKObH0eD3KurdpMkEdiEpCW12OqIArlKUGSb9O1uNqwHPZA8JVPtwqIKW2OPtlzmUQ4wmfv2-oKMQ8riOOhn9kSVEaEXUGb8VxLnu8ozjwyLFv-RVsLFGBo2x9U8ivZEKEm*K3sxecS78veb8pX6UX2PUubgMfRgN1I5O57HS3Z-PBZ2h6jQrcxtgfgeCVejM68YPU9WSE3D5qe*tcZUJMs6SMyhqxLc9WLg7ad7VdpAYKdWufMEczpWvfBVtQkGA3EN-GmCU7d9u9GUD8eumndFhb5VPvEeaAd2Zu7FIRN2h1gFu31-dH690VmykMQSkWxceNNhwScppo4btoIkq8LZTpUy8Wc3T0OVYpLDd-Hio1C6NKo5n3S*VoYzEX9ZETmK-u44KshWAHfUy57WwwyiZxZQVyuu38RgFLvffwnPterRyilr3V4A1frtFcTktbpaUGiSU2V5zc1G0VkD2gU-vYUBIeqGXjKcquRbBwGbrBW3lTRyKesYPI28*mKiY*DihuFPGrzsXqt94ZU-SVTvxhgKtoQkuNRNv*WhRZ0XRXnkmxuWbDzi74841RbP9-c-MHLi09vEq9t7krOstvzskXM2pV0TxQYmI*0y5kgRyGaK-KZrR0Dty0PaJInGrel9OAqskJWRKKeHzoHYRLFgiaxbCSnvCRlvV0qfZmvNB1VSfSB*jYwvYnUvZ3bcE*Wbac2AQ9ZLi38AR2wSVUWLKJNU9X2bUUQ-OL6dp7gzIQmqbxjmz-h6rvFh891uYfaFa3-rj8zUMsD3uhjYHe9NSZKjXE6xMk4CIbVLobJrXZKppqQGdvyGzvlinDYNtWpn1Bdb38gxMn-MJl7fGXa-VsISB-N9wIRutp2VspiGH*lFTIMLLJ187B5YqXds7y6fAZjo6KYGuPh9HKid3OYelYLGn2xZ6HkvTC1vtelgr9IOHHFH*Fz-ewxWoPlzCyC3i5fVkaLqx*t1ahmqlXQ5xltFfpbaaE9sTaLCG4CN5fHhcQi8cD5RUgq*OTsDIZFXchMJntuv5p7ZgcSFC5wCwwHfaSq3e6Ehbns1dNpF95LmUuNyb8oIWNIUK9XU86CHVdd8OBaG8cWW5fy57JWG3h42QI93RYR-eTy5lZovqXcPNjl7CZGiuaLJd94rkFvrYB8eqi-AzTqj792l2uKZP2TNYAtR7RVWaOzXdaMbV6PQT24nc0bMB1l1OfO1WvMzq32RzuemztUvyuVQTYJIHJJQWNRW-x11efS6tKBgX-l5dVQgn3ArQQWlYZSiXXJFoaACHy9vF2B6KJs5g5VI5Z501vtYqXzMBVWJoKMalkxI1EJHKlvpoqVLqnX5peZfZmSSR60HtSs1Lvd41EYLOlUqEEibMxWluG*aAgU3qcCqosPqwZZLPiysP75DuNRYuhoFSmotOChoGSq4PAWS3*ocVpWPTqb9awGQOdFcK5qe1*Vqen4UnjKpVeG08MHW7duHmR5tz5q3uk6CTz8i1U*c3F7iiJgLhfsUh7dcJVgmp3ri2ujiCIaTYO9DflvJGxXR6TntlgkftY0R8ZtPb86-Qp-coD1qpkQk1CiqZKvmXcBP6xuJXHcSW7Ctvl6DLLcJ4CAAAAuty8CgAAAAA&amp;eurl%5B%5D=CYYaGDYbJ8z8o11VMzk-6MMEEITZw5Y9-Tk056oQ1iM3ZyNIfeRxW6lIJbf5BhEBfLsWdKTp*H8fqx*925p1CNHZDtd*sp4s4omXe0RN04QCAAAAuty8CgAAAAA&amp;seq=0" scrolling="no" marginheight="0" marginwidth="0" style="width:100%;height:25px;display:inherit;" sandbox="allow-scripts allow-forms allow-popups allow-same-origin" frameborder="0"></iframe></div></div></div></div></div></div><div class=""><div class=""><div class=""><div class="_1cgiz"><div id="adfox-442305842-2" class="banner--sponsored _1gx0Z" style="margin-bottom:3px;display:block"><div id="begun_block_442305842"></div></div></div></div></div></div></div>';
// ссылка на новость для тега <a>
$link="https://website.com";
// вывод текста новости вместе со ссылкой в переменную $b
$b=news_text_shortener($a,$link,180);
// вывод $b в документ
echo $b;

// проблемы, которые могут возникнуть
/*
 * В коде тегов внутри текста новости могут оказаться ещё какие-то неучтённые мной спецсимволы, тогда они добавляются в регулярное выражение $regex на 10 строке, будучи экранированными бэкслешем "\".
 * За образец кода новости бралась новость с Рамблера - потому и поиск выполняется по символам, которые они используют
 *
 * Спецсимволы в виде каких-то специфических кавычек или иных знаков препинания, находящиеся внутри текста новости и не учтённые мной могут привести к добавлению лишних пробелов перед или после них,
 * такие спецсимполы необходимо будет добавить в preg_replace на 14й строке.
 *
 * В задании не указывалось, считать ли предлоги и союзы за слова, поэтому в этом скрипте я не реализовывал фильтр предлогов/союзов для набора текста ссылки,
 * в данном примере это выглядит не очень красиво "в ходе ..." и очень коротко.
 *
 * Также в задании не указывалось, нужно ли обрезать по словам или по символам, я считаю целесообразным выполнять обрезку по словам, если обрезать половину слова - это плохо смотрится.
 *
 */
