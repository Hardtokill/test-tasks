<?php
function imagecrop_png_custom($url='image.png',$new_file='image_resized_and_cropped.png',$width=200,$height=100,$valign=25){
    if(file_exists($url)){
        if(preg_match('/(.png)$/ui',$url)){
            $orig_image=imagecreatefrompng($url);

            $orig_image_sizes=['width'=>imagesx($orig_image),'height'=>imagesy($orig_image)];

            $new_image=imagecreatetruecolor($orig_image_sizes['width'],$orig_image_sizes['height']);

            $transparent = imagecolorallocatealpha($new_image, 0, 0, 0, 127);
            imagefill($new_image, 0, 0, $transparent);
            imagesavealpha($new_image, true);
            imagecopymerge($new_image, $orig_image, 0, 0, 0, 0, $orig_image_sizes['width'], $orig_image_sizes['height'], 100);

            imagedestroy($orig_image);

            $new_x=0;
            $new_y=0;
            imagecopyresampled($new_image, $new_image, $new_x, $new_y, 0, 0, $width /*ширина нового изображения*/, $width /*высота нового изображения*/, $orig_image_sizes['width'], $orig_image_sizes['height']);

            $new_y=$width*$valign/100; // вертикальное смещение в % от значения ширины

            $new_image=imagecrop($new_image,['x'=>$new_x,'y'=>$new_y,'width'=>$width,'height'=>$height]);

            if($new_image!==false){
                return ['result'=>true,'content'=>$new_image];
            }
            else{
                imagedestroy($new_image);
                return ['result'=>false,'content'=>'Не удалась обрезка изображения'];
            }
        }
        else{
            return ['result'=>false,'content'=>'На вход подан не png файл'];
        }
    }
    else {
        return ['result'=>false,'content'=>'Файл отсутсвует на диске либо путь указан не верно'];
    }

};
$image=imagecrop_png_custom();
if($image['result']){
    //header('Content-Type: image/png');
    //imagepng($image['content']);
    //imagedestroy($image['content']);
}else{
    echo $image['content'];
}
echo memory_get_peak_usage();