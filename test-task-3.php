<?php
$mysql=['ip'=>'127.0.0.1:3306','login'=>'admin','password'=>'root','database'=>'test'];
$mysqli = mysqli_connect($mysql['ip'], $mysql['login'], $mysql['password'], $mysql['database']);
$mysqli->set_charset("utf8mb4");
$mysqli_query = 'SELECT * FROM a, b WHERE a.id=b.a_id';
$time_start = microtime(true);
$result_1=$mysqli->query($mysqli_query);
$time_end = microtime(true);
$time = $time_end - $time_start;
echo 'Первый запрос длился '.$time.' сек.<br>';
$time_start = microtime(true);
$mysqli_query = 'SELECT * FROM a JOIN b ON a.id=b.a_id';
$result_2=$mysqli->query($mysqli_query);
$time_end = microtime(true);
$time = $time_end - $time_start;
echo 'Второй запрос длился '.$time.' сек.<br>';
var_dump($result_1->fetch_all());
echo '<br>';
var_dump($result_2->fetch_all());
// результаты в обоих случаях мы получаем одинаковые
// первый запрос выполняется медленнее первого, в силу большего кол-ва переборов второй таблицы